# aosp-mirror-creator

aosp镜像创建，可用于搭建本地镜像，减少网络流量

##  如何建立次级镜像
[点击查看](https://mirrors.tuna.tsinghua.edu.cn/help/AOSP/#%E5%BB%BA%E7%AB%8B%E6%AC%A1%E7%BA%A7%E9%95%9C%E5%83%8F)

##  可替代的其他镜像源

1.  [清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/help/AOSP/)
    ```shell
    repo init -u https://aosp.tuna.tsinghua.edu.cn/mirror/manifest --mirror
    ```
1.  [中国科学技术大学镜像](https://mirrors.ustc.edu.cn/)
    ```shell
    repo init -u https://mirrors.ustc.edu.cn/aosp/mirror/manifest --mirror
    ```
1.  [高通镜像源](https://source.codeaurora.org/quic/la)
    ```shell
    repo init -u https://source.codeaurora.org/quic/la/mirror/manifest --mirror
    ```
1.  [`googlesource`（国内禁止访问）](https://android.googlesource.com)
    ```shell
    repo init -u https://android.googlesource.com/mirror/manifest --mirror
    ```


## `repo sync`出现错误？
1.  `https`证书错误:需要修改`repo`文件
    ```python
    # 修复https引发的bug
    import ssl
    ssl._create_default_https_context = ssl._create_unverified_context
    ```

1.  `error: Cannot fetch [project]`错误
    ```shell
    error: Cannot fetch platform/prebuilts/ndk from https://aosp.tuna.tsinghua.edu.cn/platform/prebuilts/ndk
    error: RPC failed; curl 56 GnuTLS recv error (-54): Error in the pull function.64% (68461/106266), 2.68 GiB | 237.00 KiB/s
    fatal: The remote end hung up unexpectedly
    fatal: early EOF
    fatal: index-pack failed
    device/lge/hammerhead-kernel:
    remote: Counting objects: 4800, done.
    error: RPC failed; curl 56 GnuTLS recv error (-54): Error in the pull function.
    fatal: The remote end hung up unexpectedly
    fatal: early EOF
    fatal: index-pack failed
    ```

    暂时没什么好的解决方案，使用`./repo_cn sync -j8 | ./fix_repo_sync`或`./repo_sync`进行自动收集出现错误的模块并重新同步

1.  `clone`出现中断：试试增加`postBuffer`的值
    ```shell
    # 查看当前设置
    git config --list
    # 例：设置5GB缓存区
    git config --global http.postBuffer 5242880000
    git config --global https.postBuffer 5242880000
    git config --global https.sslverify false
    ```

##  `git`运行时替换`android.googlesource.com`
```shell
git config --global url.[需要替换的域名].insteadof https://android.googlesource.com
```
1.  [清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/help/AOSP/)
    ```shell
    git config --global url.https://mirrors.tuna.tsinghua.edu.cn/git/AOSP/.insteadof https://android.googlesource.com
    ```
1.  [中国科学技术大学镜像](https://mirrors.ustc.edu.cn/)
    ```shell
    git config --global url.https://mirrors.ustc.edu.cn/aosp/.insteadof https://android.googlesource.com
    ```
1.  [高通镜像源](https://source.codeaurora.org/quic/la)
    ```shell
    git config --global url.https://source.codeaurora.org/quic/la/.insteadof https://android.googlesource.com
    ```